//Own includes
#include "png.h"
#include "rgbapixel.h"
#include "encrypt.h"

#include <string>

//function to encrypt via stegonography 
/** first attempt, idea is to directly add the alpha of the string
	to the image on top. --- completed
*/
PNG* encrypt( PNG* pngToEncrypt, string storage )
{
	//initialize random seed
	srand(time(NULL));

	//prevent multiple calls
	int strLength = storage.length();
	int xMax = pngToEncrypt->width();
	int yMax = pngToEncrypt->height();

	if(xMax * yMax >= strLength)
	{
		//use that length to adjust the picture by that much
		for(int i = 0; i < strLength; i++)
		{
			//skip to the next line once the end of the pixel row is reached
			int val = storage[i] - '0';
			(*pngToEncrypt)(i%xMax,i/xMax)->alpha = 255 - val;
		}
	}

	return pngToEncrypt;
}