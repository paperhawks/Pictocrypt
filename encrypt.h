#ifndef _ENCRYPT_H
#define _ENCRYPT_H

#include <png.h>

PNG* encrypt( PNG* pngToEncrypt, string storage );

#endif