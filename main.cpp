//Own includes
#include "png.h"
#include "rgbapixel.h"
#include "encrypt.h"
#include "decrypt.h"

//standard includes
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <algorithm> //initially for string transform
#include <bitset>

using namespace std;

//convert string to binary so that the increases are just 1 at most.
string convertToBinary( string stringToConvert )
{
	string converted = "";

	//runs through each char and converts it to an 8 bit bitset
	//bitset is then converted to string and concatenated
	for(int i = 0; i < stringToConvert.size(); i++)
	{
		string stringBit = 
		bitset<8>(stringToConvert.c_str()[i]).to_string<char,std::string::traits_type,std::string::allocator_type>();
		converted = converted + stringBit;
	}

	return converted;
}

//converts each bit string given back to a char
string bitSetToChar( string bits )
{
	string convertedString = "";
	//work on every char
	for(int count = 0; count < bits.size()/8; count++)
	{
		bitset<8> tempBits;
		for( int i = 0; i < 8; i++)
		{
			//first convert each 8 bit part of the string into 8 bit bitsets
			//account for the fact that bits start from the right, not the left
			tempBits.set(7-i,bits[count*8 + i] - '0');
		}

		//converts to an unsigned long and takes that to map to char
		unsigned long longChar = tempBits.to_ulong();
		unsigned char convertedChar = static_cast<unsigned char>(longChar);
		convertedString.push_back(convertedChar);
	}
	return convertedString;
}

//main program -- checks to see decode or encode
int main()
{
	//determine user input
	string userInput = "";
	string imgName = "";
	string useImage = "";
	PNG* inputImg; 
	cout << "Press \" e\" to encrypt, \"d\" decrypt." << endl;
	cout << "Operation:";
	getline(cin, userInput);
	
	PNG* outputImg;

	//run user demand
	if( userInput.compare("e") == 0 )
	{
		//determine whether there is an prefered image
		cout << "Do you have an image you want to use? [Y/n]";
		getline(cin, useImage);

		if( useImage.compare("n") == 0 ||
			useImage.compare("N") == 0 ||
			useImage.compare("No") == 0 ||
			useImage.compare("no") == 0)
		{
			//create a blank image should be be on specific file to use
			PNG blankImg(100,100);

			//determine the string to store
			string lineToStore = "";
			cout << "What is the string you wish to store?";
			getline(cin, lineToStore);

			//convert line to binary and then run the encryption
			lineToStore = convertToBinary(lineToStore);
			outputImg = encrypt( &blankImg, lineToStore );
			outputImg->writeToFile("out.png");
		}
		else
		{
			//determine what the image input file is
			cout << "What is the image name (neglect extension)?";
			getline(cin, imgName);

			//change here to reflect user input image
			PNG userImg(imgName + ".png");

			//determine the string to store
			string lineToStore = "";
			cout << "What is the string you wish to store?";
			getline(cin, lineToStore);

			//convert line to binary and then run the encryption
			lineToStore = convertToBinary(lineToStore);
			outputImg = encrypt( &userImg, lineToStore );
			outputImg->writeToFile("out.png");
		}
	}

	if( userInput.compare("d") == 0 )
	{
		//determine what the output file is
		cout << "What is the image name (neglect extension)?";
		getline(cin, imgName);

		//change here to reflect user input image
		PNG userImg(imgName + ".png");
		inputImg = &userImg;

		//decrypt
		string decryptedStr = bitSetToChar(decrypt( inputImg ));
		cout << decryptedStr << endl;
	}

	//free memory to prevent memory leaks
	outputImg = NULL;
	inputImg = NULL;

	return 0;
}