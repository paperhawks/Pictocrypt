#ifndef _DECRYPT_H
#define _DECRYPT_H

#include <string>
#include "png.h"

using std::string;

string decrypt( PNG* pngToDecrypt );

#endif