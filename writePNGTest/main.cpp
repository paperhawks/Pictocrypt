//Own includes
#include "png.h"
#include "rgbapixel.h"

//standard includes
#include <iostream>
#include <string>
#include <sstream>
#include <vector>

using namespace std;

//function to test whether simple writes of ints to picture works
int main()
{
	//create an array of strings and then store them into some png
	vector<int> v;
	PNG pic("out.png");
	for(int i = 0; i<10; i++)
	{
		v.push_back(i);
		//for(int x = 0; x<pic.width(); x++)
		pic(i,0)->alpha = v[i];
		cout << "v" << v[i] << endl;
		cout << "stored" << (int)pic(i,0)->alpha << endl;
	}

	//read from png
	vector<int> vOut;
	for(int i = 0; i < v.size(); i++)
	{
		vOut.push_back((int)pic(i,0)->alpha);
		//check output
		cout << "vout" << vOut[i] << endl;
	}
	return 0;
}