EXENAME = pictocrypt

COMPILER = g++
WARNINGS = -Wchar-subscripts -Wparentheses -Wreturn-type -Wmissing-braces -Wundef -Wshadow
COMPILER_OPTS = -c -g -O0 -Wfatal-errors -Werror $(WARNINGS)
LINKER = g++

MAIN_OBJS = main.o png.o rgbapixel.o encrypt.o decrypt.o
MAIN_DEPS = png.o rgbapixel.o encrypt.o decrypt.o main.cpp

EPNG_DEPS = rgbapixel.o png.h png.cpp
BPNG_DEPS = png.o 
RGBAPIXEL_DEPS = png.h rgbapixel.h rgbapixel.cpp
CODE_CLN = *.o $(EXENAME)

all: $(EXENAME)

$(EXENAME) : main.o
	$(LINKER) $(MAIN_OBJS) -lpng -o $(EXENAME)

main.o : $(MAIN_DEPS)
	$(COMPILER) $(COMPILER_OPTS) main.cpp

encrypt.o : png.o encrypt.h encrypt.cpp
	$(COMPILER) $(COMPILER_OPTS) encrypt.cpp

decrypt.o : png.o decrypt.h decrypt.cpp
	$(COMPILER) $(COMPILER_OPTS) decrypt.cpp

png.o : $(EPNG_DEPS)
	$(COMPILER) $(COMPILER_OPTS) png.cpp

rgbapixel.o : $(RGBAPIXEL_DEPS)
	$(COMPILER) $(COMPILER_OPTS) rgbapixel.cpp



clean:
	-rm -f $(CODE_CLN) $(IMAG_CLN)
