#include "decrypt.h"

//function to decrypt
//first attempt is proof of concept, to make sure it can decrypt a limited number
string decrypt( PNG* pngToDecrypt )
{
	string decryptedStr = "";
	int xMax = pngToDecrypt->width();
	int yMax = pngToDecrypt->height();

	//run through the first n pixels and read
	for( int i = 0; i < xMax * yMax; i++ )
	{
		char temp = (char)((255 - (*pngToDecrypt)(i%xMax,i/xMax)->alpha) + '0');
		//test in accordance to ascii table
		if(temp >= ' ' && temp <= '~')
			decryptedStr.push_back(temp);
	}

	return decryptedStr;
}